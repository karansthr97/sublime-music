.. image:: https://gitlab.com/sumner/sublime-music/-/raw/master/logo/logo.png
   :alt: Sublime Music Logo

Sublime Music is a GTK3
`Subsonic`_/`Airsonic`_/`Revel`_/`Gonic`_/`Navidrome`_/\*sonic client for the
Linux Desktop.

.. _Subsonic: http://www.subsonic.org/pages/index.jsp
.. _Airsonic: https://airsonic.github.io/
.. _Revel: https://gitlab.com/robozman/revel
.. _Gonic: https://github.com/sentriz/gonic
.. _Navidrome: https://www.navidrome.org/

-------------------------------------------------------------------------------

|userdoc|_

.. |userdoc| replace:: **Click HERE for extended user documentation.**
.. _userdoc: https://sumner.gitlab.io/sublime-music/

See the |contributing|_ document for how to contribute to this project.

.. |contributing| replace:: ``CONTRIBUTING.rst``
.. _contributing: https://gitlab.com/sumner/sublime-music/-/blob/master/CONTRIBUTING.rst

You can also join the conversation in our Matrix room:
`#sublime-music:matrix.org <https://matrix.to/#/!veTDkgvBExJGKIBYlU:matrix.org?via=matrix.org>`_.
